#include <random>
#include <chrono>
#include "matmul.h"

using namespace std;

using std::chrono::duration;
using std::chrono::high_resolution_clock;

void populateMatrices(float *matrix, int n)
{
    /**
     * matrix is the pointer to the matrix
     * n is the number of rows (and columns)
     *
     */
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<float> dist(0, 10);
    for (size_t i = 0; i < n * n; i++)
    {
        matrix[i] = dist(gen);
    }
}

void clearOutput(float *matrix, int n)
{
    for (size_t i = 0; i < n * n; i++)
    {
        matrix[i] = 0;
    }
}

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 3)
    {
        printf("Usage: ./task1 n num_threads");
        return 0;
    }

    int n = atoi(argv[1]);
    int t = atoi(argv[2]);
    omp_set_num_threads(t);

    high_resolution_clock::time_point start;
    high_resolution_clock::time_point end;
    duration<float, std::milli> duration_sec;

    float *A = (float *)malloc(sizeof(float) * n * n);
    float *B = (float *)malloc(sizeof(float) * n * n);
    float *C = (float *)malloc(sizeof(float) * n * n);

    populateMatrices(A, n);
    populateMatrices(B, n);

    start = high_resolution_clock::now();
    mmul(A, B, C, n);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
    printf("%f\n%f\n%f\n", C[0], C[n * n - 1], duration_sec.count());
    // printf("(%d, %f),\n", t, duration_sec.count());

    free(A);
    free(B);
    free(C);
    return 0;
}
