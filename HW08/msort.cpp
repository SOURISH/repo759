#include <stdio.h>
#include <algorithm>
#include "msort.h"

void msort(int *arr, const std::size_t n, const std::size_t threshold)
{
#pragma omp parallel
    {
#pragma omp single nowait // Only have one task put the post its on the board
        {
            if (n < threshold)
            {
                // Serial sort
                std::sort(arr, arr + n);
            }
            else
            {
                // Parallel sort
                int *arr1 = arr;
                int *arr2 = &arr[n / 2];
#pragma omp task // Put the left half of the merge sort on the post-it board
                msort(arr1, n / 2, threshold);
#pragma omp task // Put the right half of the merge sort on the post-it board
                msort(arr2, n - n / 2, threshold);
#pragma omp taskwait // Wait for the left and right half to finish
                std::inplace_merge(arr1, arr1 + n / 2, arr1 + n);
            }
        }
    }
}