#include <cstdlib>
#include <stdlib.h>
#include <chrono>
#include <random>
#include "convolution.h"

using namespace std;

using std::chrono::duration;
using std::chrono::high_resolution_clock;

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 3)
    {
        printf("Usage: ./task1 n num_threads");
        return 0;
    }

    int n = atoi(argv[1]);
    int t = atoi(argv[2]);
    omp_set_num_threads(t);

    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<float> dist_image(-10, 10);
    uniform_real_distribution<float> dist_mask(-1, 1);

    high_resolution_clock::time_point start;
    high_resolution_clock::time_point end;
    duration<double, std::milli> duration_sec;

    int m = 3;

    float * image = (float *) malloc(sizeof(float) * n * n);
    float * output = (float *) malloc(sizeof(float) * n * n);
    float * mask = (float *) malloc(sizeof(float) * m * m);

    for (size_t i = 0; i < n * n; i++)
    {
        image[i] = dist_image(gen);
    }

    for (size_t i = 0; i < m * m; i++)
    {
        mask[i] = dist_mask(gen);
    }

    start = high_resolution_clock::now();
    convolve(image, output, n, mask, m);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli> >(end - start);
    printf("%f\n%f\n%f\n", output[0], output[n * n - 1], duration_sec.count());
    // printf("(%d, %f),\n", t, duration_sec.count());

    free(image);
    free(output);
    free(mask);
    return 0;
}
