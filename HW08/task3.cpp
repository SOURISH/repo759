#include <cstdlib>
#include <stdlib.h>
#include <chrono>
#include <random>
#include "msort.h"

using namespace std;

using std::chrono::duration;
using std::chrono::high_resolution_clock;

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 4)
    {
        printf("Usage: ./task1 n num_threads threashold");
        return 0;
    }

    int n = atoi(argv[1]);
    int t = atoi(argv[2]);
    int threshold = atoi(argv[3]);
    omp_set_num_threads(t);

    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dist(-1000, 1000);

    high_resolution_clock::time_point start;
    high_resolution_clock::time_point end;
    duration<double, std::milli> duration_sec;

    int *A = (int *)malloc(sizeof(int) * n);

    // populate A with random numbers
    for (int i = 0; i < n; i++)
    {
        A[i] =  (int) dist(gen);
    }

    start = high_resolution_clock::now();
    msort(A, n, threshold);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli>>(end - start);
    printf("%d\n%d\n%f\n", A[0], A[n - 1], duration_sec.count());
    // printf("(%d, %f),\n", t, duration_sec.count());

    free(A);
    return 0;
}
