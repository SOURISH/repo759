#include <omp.h>
#include <stdio.h>
#include "matmul.h"

void mmul(const float *A, const float *B, float *C, const std::size_t n)
{
int n_int = (int)n;
#pragma omp parallel for collapse(3)
    for (int i = 0; i < n_int; i++)
    {
        for (int k = 0; k < n_int; k++)
        {
            for (int j = 0; j < n_int; j++)
            {
#pragma omp atomic
                C[i * n_int + j] += A[i * n_int + k] * B[k * n_int + j];
            }
        }
    }
}