#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/random.h>


int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 2)
    {
        printf("Usage: ./task1_thrust n");
        return 0;
    }
    int n = atoi(argv[1]);

    // Creating a host and device vector, populating the host vector with random numbers, and copying it to the device vector
    thrust::host_vector<float> h_vec(n);
    thrust::device_vector<float> d_vec(n);

    thrust::default_random_engine rng;
    thrust::uniform_real_distribution<float> dist(-1, 1);
    
    thrust::generate(h_vec.begin(), h_vec.end(), [&] { return dist(rng); });
    thrust::copy(h_vec.begin(), h_vec.end(), d_vec.begin());

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);

    float result = thrust::reduce(d_vec.begin(), d_vec.end(), 0.0f, thrust::plus<float>());
    
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    float ms = 0;
    cudaEventElapsedTime(&ms, start, stop);
    printf("%f\n%f\n", result, ms);
    return 0;
}
