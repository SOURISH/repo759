#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/random.h>
#include "count.cuh"


int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 2)
    {
        printf("Usage: ./task2 n");
        return 0;
    }
    int n = atoi(argv[1]);

    // Creating a host and device vector, populating the host vector with random numbers, and copying it to the device vector
    thrust::host_vector<int> h_vec(n);
    thrust::device_vector<int> d_vec(n);

    thrust::default_random_engine rng;
    thrust::uniform_real_distribution<float> dist(0, 501);
    
    thrust::generate(h_vec.begin(), h_vec.end(), [&] { return dist(rng); });
    
    d_vec = h_vec;

    thrust::device_vector<int> values;
    thrust::device_vector<int> counts;

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);

    count(d_vec, values, counts);
    
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    float ms = 0;
    cudaEventElapsedTime(&ms, start, stop);

    // Copying the last elements of the values and counts vectors to the host
    int last_elem_vals;
    int last_elem_counts;
    thrust::copy(values.end() - 1, values.end(), &last_elem_vals);
    thrust::copy(counts.end() - 1, counts.end(), &last_elem_counts);
    printf("%d\n%d\n%f\n", last_elem_vals, last_elem_counts, ms);
    return 0;
}
