#include <iostream>
#include <omp.h>

// Helper function to calculate the factorial
int factorial(int num) {
    int result = 1;
    for (int i = 1; i <= num; i++)
    {
        result *= i;
    }
    return result;
}

int main(int argc, char const *argv[])
{
    int num_of_threads = 4;
    omp_set_num_threads(num_of_threads);
    printf("Number of threads:  %d\n", num_of_threads);
    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        printf("I am thread No.  %d\n", id);
        #pragma omp barrier // Required so all threads print their thread number first
        
        // Thread 0 gets mapped to 1 and 2, Thread 1 gets mapped to 3 and 4, etc.
        int i = id * 2 + 1;
        int j = id * 2 + 2;
        printf("%d!=%d\n", i, factorial(i));
        printf("%d!=%d\n", j, factorial(j));
    }
    return 0;
}
