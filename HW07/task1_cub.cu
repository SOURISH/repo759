#define CUB_STDERR
#include <stdio.h>
#include <random>
#include <cub/util_allocator.cuh>
#include <cub/device/device_reduce.cuh>
#include "cub/util_debug.cuh"
cub::CachingDeviceAllocator  g_allocator(true);  // Caching allocator for device memory

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 2)
    {
        printf("Usage: ./task1_thrust n");
        return 0;
    }
    int n = atoi(argv[1]);

    // Creating random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dist(-1, 1);
    
    // Set up host arrays
    float *h_in = new float[n];
    for (int i = 0; i < n; i++)
    {
        h_in[i] = dist(gen);
    }

    // Set up device arrays
    float* d_in = NULL;
    g_allocator.DeviceAllocate((void**)& d_in, sizeof(float) * n);
    // Initialize device input
    cudaMemcpy(d_in, h_in, sizeof(float) * n, cudaMemcpyHostToDevice);
    // Setup device output array
    float* d_sum = NULL;
    g_allocator.DeviceAllocate((void**)& d_sum, sizeof(float) * 1);
    // Request and allocate temporary storage
    void* d_temp_storage = NULL;
    size_t temp_storage_bytes = 0;
    cub::DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_in, d_sum, n);
    g_allocator.DeviceAllocate(&d_temp_storage, temp_storage_bytes);

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    
    // Do the actual reduce operation
    cub::DeviceReduce::Sum(d_temp_storage, temp_storage_bytes, d_in, d_sum, n);

    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    float gpu_sum;
    cudaMemcpy(&gpu_sum, d_sum, sizeof(float) * 1, cudaMemcpyDeviceToHost);
    // Check for correctness

    float ms = 0;
    cudaEventElapsedTime(&ms, start, stop);
    printf("%f\n%f\n", gpu_sum, ms);

    // Cleanup
    if (d_in) g_allocator.DeviceFree(d_in);
    if (d_sum) g_allocator.DeviceFree(d_sum);
    if (d_temp_storage) g_allocator.DeviceFree(d_temp_storage);
    return 0;
}
