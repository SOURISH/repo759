#include <stdio.h>
#include "scan.cuh"

/*
This function uses shared memory so it only performs scan on a single block.
There is no cross block communication.
At the end of the scan, the last element is stored in an offsets array for future use
*/
__global__ void hillis_steele(const float *g_idata, float *g_odata, int n, float *offsets)
{
    extern volatile __shared__ float temp[]; // allocated on invocation

    int idx = threadIdx.x;
    // If n is not an even multiple of blockDim.x, make the last block's size the number of elements in input array left
    // Otherwise, set n to the blockDim (because that's the maximum number of elements this block will process)
    n = (blockIdx.x == gridDim.x - 1 && n % blockDim.x != 0) ? n % blockDim.x : blockDim.x;

    if (idx >= n)
        return;

    int pout = 0, pin = 1;

    // load input into shared memory.
    temp[idx] = g_idata[blockDim.x * blockIdx.x + idx];
    __syncthreads();

    for (int offset = 1; offset < n; offset *= 2)
    {
        pout = 1 - pout; // swap double buffer indices
        pin = 1 - pout;

        if (idx >= offset)
            temp[pout * n + idx] = temp[pin * n + idx] + temp[pin * n + idx - offset];
        else
            temp[pout * n + idx] = temp[pin * n + idx];

        __syncthreads(); // I need this here before I start next iteration
    }

    g_odata[blockDim.x * blockIdx.x + idx] = temp[pout * n + idx]; // write output

    // Checking if current block is not last block in grid and if current thread is last thread in block
    // If true, store the last element of result in offset array
    if (blockIdx.x != gridDim.x - 1 && idx == n - 1)
        offsets[blockIdx.x + 1] = temp[pout * n + idx];
}

/*
Kernel call that performs one iteration of the hillis_steele scan algorithm.
*/
__global__ void hillis_steele_2_helper(float *data, int n, int offset, int pin, int pout)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= n)
        return;

    if (idx >= offset)
        data[pout * n + idx] = data[pin * n + idx] + data[pin * n + idx - offset];
    else
        data[pout * n + idx] = data[pin * n + idx];
}

/*
Helper function to swap the values in the first half and second half of the array
*/
__global__ void swap(float *data, int n)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= n)
        return;

    float temp = data[idx];
    data[idx] = data[idx + n];
    data[idx + n] = temp;
}

/*
This scan implementation forgoes shared memory and uses global memory instead.
This is because the input size may be greater than the block size.
*/
__host__ void hillis_steele_global_mem(float *data, int n, int threads_per_block)
{
    int pout = 0, pin = 1;
    int num_of_blocks = (n + (int)threads_per_block - 1) / (int)threads_per_block;

    for (int offset = 1; offset < n; offset *= 2)
    {
        pout = 1 - pout;
        pin = 1 - pout;

        hillis_steele_2_helper<<<num_of_blocks, threads_per_block>>>(data, n, offset, pin, pout);
    }

    // Making sure all of the correct values are in the first half of the array
    if (pout == 1)
        swap<<<num_of_blocks, threads_per_block>>>(data, n);
}

/*
This function adds the values in the offsets array to get the correct results of the scan algorithm
*/
__global__ void apply_offsets(float *data, int n, float *offsets)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= n)
        return;
    data[idx] += offsets[blockIdx.x];
}

/*
This function performs a scan on the input array using the hillis_steele algorithm
*/
__host__ void scan(const float *input, float *output, unsigned int n, unsigned int threads_per_block)
{
    int signed_N = n;
    int num_of_blocks = (signed_N + (int)threads_per_block - 1) / (int)threads_per_block;

    /*
    This is used to calculate the correct offsets for each block. The first call to the hillis_steele kernel will only perfrom scan on each block in isolation. This means each of the elements in a block after the first one will be off by a constant offset. For example, each of the elements in block 2 will be off by the last element of block 1. Assuming block size of 32, the number at index value 63 (the last element of block 2) only contains the sum of the elements 32 through 63. This means we still need to add the value of the sum of elements 0 through 31 to get the correct value. Conveniently, the last element of each block contains the sum of all the elements in that block. Similarly, each of the elements in block 3 will be off by the last element of block 2 plus the last element of block 1, and so on.
    */
    int deviceId;
    cudaGetDevice(&deviceId);
    float *offsets;
    cudaMallocManaged(&offsets, sizeof(float) * num_of_blocks * 2);
    cudaMemsetAsync(offsets, 0, sizeof(float) * num_of_blocks * 2);
    cudaMemPrefetchAsync(offsets, sizeof(float) * num_of_blocks * 2, deviceId);

    /*
    After this kernel call, each of the blocks will have the scan operation performed on it, but the results are only contained to the number of elements in each block!
    (Assuming block size of 1024) Element 2047 (the last block in the second block) will only hold the sum of elements 1024 through 2047, not 0 through 2047 as intended.
    */
    hillis_steele<<<num_of_blocks, threads_per_block, 2 * threads_per_block * sizeof(float)>>>(input, output, n, offsets);

    /*
    For the second scan call, we'll need as many threads as there were blocks in the previous call. This is because we have one offset value from each block and we need to perform the scan operation on this new array of values
    */
    hillis_steele_global_mem(offsets, num_of_blocks, threads_per_block);

    /*
    After we have the values representing by how much each block's values are off by, we'll need to apply this to the output array. This will give us the correct values for each element in the array.
    */
    apply_offsets<<<num_of_blocks, threads_per_block>>>(output, n, offsets);
    cudaDeviceSynchronize();

    cudaFree(offsets);
}
