#include <cuda.h>
#include <stdio.h>
#include <random>
#include "scan.cuh"

__host__ void hillis_steele_test();
__host__ void hillis_steele_2_test();

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 3)
    {
        printf("Usage: ./task2 n threads_per_block");
        return 0;
    }

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // Grabbing device information for copying data between pointers
    int deviceId;
    cudaGetDevice(&deviceId);

    float *input;
    float *output;

    int n = atoi(argv[1]);
    int num_of_threads = atoi(argv[2]);

    cudaMallocManaged(&input, sizeof(float) * n);
    cudaMallocManaged(&output, sizeof(float) * n);

    // Creating random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dist(-1, 1);

    // Initalizing the input array with random numbers
    for (int i = 0; i < n; i++)
        input[i] = dist(gen);

    cudaMemPrefetchAsync(input, n, deviceId);
    cudaMemPrefetchAsync(output, n, deviceId);

    // Starting timer and calling kernel
    cudaEventRecord(start);
    scan(input, output, n, num_of_threads);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Measuring time elapsed
    float ms;
    cudaEventElapsedTime(&ms, start, stop);
    printf("%f\n%f\n", output[n - 1], ms);

    // Freeing memory
    cudaFree(input);
    cudaFree(output);
    return 0;
}
