#include <cuda.h>
#include <cublas_v2.h>
#include <stdio.h>
#include <random>
#include "mmul.h"


// using namespace std;

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 3)
    {
        printf("Usage: ./task1 n n_tests");
        return 0;
    }

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // Grabbing device information for copying data between pointers
    int deviceId;
    cudaGetDevice(&deviceId);

    cublasHandle_t handle;
    cublasCreate(&handle);

    float *A;
    float *B;
    float *C;

    int n = atoi(argv[1]);
    int size = n * n;
    int n_tests = atoi(argv[2]);

    cudaMallocManaged(&A, sizeof(float) * size);
    cudaMallocManaged(&B, sizeof(float) * size);
    cudaMallocManaged(&C, sizeof(float) * size);

    // Creating random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dist(-1, 1);

    // Initializing the two matrices that'll be multiplied in column major order 
    for (int i = 0; i < size; i++) {
        A[i] = dist(gen);
        B[i] = dist(gen);
    }

    cudaMemPrefetchAsync(A, size, deviceId);
    cudaMemPrefetchAsync(B, size, deviceId);
    cudaMemPrefetchAsync(C, size, deviceId);

    float sum = 0;
    for (int i = 0; i < n_tests; i++)
    {
        // Starting timer and calling kernel
        cudaEventRecord(start);
        mmul(handle, A, B, C, n);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);

        // Measuring time elapsed
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        sum += ms;
    }
    sum /= n_tests;
    printf("%f\n", sum);

    cudaFree(A);
    cudaFree(B);
    cudaFree(C);
    return 0;
}

