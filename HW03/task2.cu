#include <cuda.h>
#include <stdio.h>
#include <random>

using namespace std;

// For each value in dA, this function multiplies it by the threadIdx and adds the blockIdx
__global__ void affineTransformation(int *dA, int a)
{
    int num = blockIdx.x * blockDim.x + threadIdx.x;
    int x = threadIdx.x;
    int y = blockIdx.x;
    dA[num] = a * x + y;
}

int main()
{  
    // Alocating memory for array
    const int numElems = 16;
    int *dA;
    int *hA;
    cudaMalloc(&dA, sizeof(int) * numElems);
    cudaMallocHost(&hA, sizeof(int) * numElems);

    // Creating random number generator
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dist(-100, 100);

    // Calling the kernel after randomly generating a
    int a = dist(gen);
    affineTransformation<<<2, 8>>>(dA, a);
    cudaDeviceSynchronize();

    // Copying information from device to host pointer, then printing it
    cudaMemcpy(hA, dA, sizeof(int) * numElems, cudaMemcpyDeviceToHost);
    for (size_t i = 0; i < numElems; i++)
    {
        printf("%d ", hA[i]);
    }
    printf("\n");

    // Freeing pointers
    cudaFree(dA);
    cudaFree(hA);

    return 0;
}