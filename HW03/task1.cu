#include <cuda.h>
#include <stdio.h>

__global__ void computeFactorial()
{
    // Grabbing the factorial base, adding one is required because the current range is from 0 to 7
    int num = blockIdx.x * blockDim.x + threadIdx.x + 1;

    // For loop to iterate through each of the multiplicands in factorial operator
    int total = 1;
    for (int i = 1; i < num + 1; i++)
    {
        total *= i;
    }

    printf("%d!=%d\n", num, total);
}

int main()
{
    const int numElems = 8;
    computeFactorial<<<1, numElems>>>(); // Calling kernel with 1 block and 8 threads
    cudaDeviceSynchronize();
    return 0;
}