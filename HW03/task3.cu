#include <cuda.h>
#include <stdio.h>
#include <random>
#include "vscale.cuh"

using namespace std;

#define BLOCK_SIZE 512

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 2) {
        printf("Usage: ./task3 n");
        return 0;
    }
    int deviceId;
    cudaGetDevice(&deviceId);

    // Creating random number generators
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<float> dist_a(-10, 10);
    uniform_real_distribution<float> dist_b(0, 1);

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // Converting user input to int to create arrays of appropriate size
    int n = atoi(argv[1]);

    // Allocating memory for a and b pointers
    float *a;
    float *b;
    cudaMallocManaged(&a, sizeof(float) * n);
    cudaMallocManaged(&b, sizeof(float) * n);

    // Generating random values for each element in a and b
    for (int i = 0; i < n; i++)
    {
        a[i] = dist_a(gen);
        b[i] = dist_b(gen);
    }

    // Prefetching to GPU to prevent page faulting
    cudaMemPrefetchAsync(a, sizeof(float) * n, deviceId);
    cudaMemPrefetchAsync(b, sizeof(float) * n, deviceId);

    // Starting timer and calling kernel
    cudaEventRecord(start);
    vscale<<<n % BLOCK_SIZE, BLOCK_SIZE>>>(a, b, n);
    cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Measuring time elapsed
    float ms;
    cudaEventElapsedTime(&ms, start, stop);

    printf("%f\n%f\n%f", ms, b[0], b[n - 1]);

    // Freeing pointers
    cudaFree(a);
    cudaFree(b);

    return 0;
}