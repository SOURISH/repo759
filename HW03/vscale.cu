#include <cuda.h>
#include <stdio.h>
#include "vscale.cuh"

__global__ void vscale(const float *a, float *b, unsigned int n) {
    int num = blockIdx.x * blockDim.x + threadIdx.x;
    if (num >= n) return;
    b[num] *= a[num]; // Taking the element in b and multiplying it by the corresponding element in a
}