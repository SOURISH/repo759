#include <cuda.h>
#include <stdio.h>
#include <random>
#include "matmul.cuh"

using namespace std;

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 3) {
        printf("Usage: ./task1 n threads_per_block");
        return 0;
    }

    // Grabbing device information for copying data between pointers
    int deviceId;
    cudaGetDevice(&deviceId);

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    float *A;
    float *B;
    float *C;

    int n = atoi(argv[1]);
    int threads_per_block = atoi(argv[2]);
    int size = n * n;

    cudaMalloc(&A, sizeof(float) * size);
    cudaMalloc(&B, sizeof(float) * size);
    cudaMalloc(&C, sizeof(float) * size);

    float *hA;
    float *hB;
    float *hC;

    cudaMallocHost(&hA, sizeof(float) * size);
    cudaMallocHost(&hB, sizeof(float) * size);
    cudaMallocHost(&hC, sizeof(float) * size);

    // Creating random number generator
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dist(-1, 1);

    // Initalizing the two matrices that'll be multiplied
    for (int i = 0; i < size; i++)
    {
        hA[i] = dist(gen);
        hB[i] = dist(gen);
    }
    
    // Copying randomly generated numbers to device
    cudaMemcpy(A, hA, sizeof(float) * size, cudaMemcpyHostToDevice);
    cudaMemcpy(B, hB, sizeof(float) * size, cudaMemcpyHostToDevice);

    // Starting timer and calling kernel
    cudaEventRecord(start);
    matmul(A, B, C, n, threads_per_block);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Measuring time elapsed
    float ms;
    cudaEventElapsedTime(&ms, start, stop);

    // Copying result to host
    cudaMemcpy(hC, C, sizeof(float) * size, cudaMemcpyDeviceToHost);

    printf("%f\n%f\n", hC[size - 1], ms);
    
    cudaFree(A);
    cudaFree(B);
    cudaFree(C);
    cudaFree(hA);
    cudaFree(hB);
    cudaFree(hC);
    return 0;
}
