#include "stencil.cuh"

// https://developer.nvidia.com/blog/using-shared-memory-cuda-cc/

__global__ void stencil_kernel(const float *image, const float *mask, float *output, unsigned int n, unsigned int R)
{
    int signed_R = (int)R;
    
    extern __shared__ float s[];
    float *image_shared = s; // Is of length num_of_threads + 2 * R
    // blockDim.x represents the number of threads in this block
    float *mask_shared = (float *) &s[blockDim.x + 2 * signed_R]; // Is of size 2 * R + 1
    float *output_shared = (float *) &mask_shared[2 * signed_R + 1];

    int i = threadIdx.x + blockIdx.x * blockDim.x;

    // Moving image over to shared memory
    if (i < n)
    {
        // Each thread will move at least one element over to the shared array
        image_shared[threadIdx.x + signed_R] = image[i];

        if (threadIdx.x < signed_R) // Checking for the first R threads to move the extra R values at the beginning
        {
            if (i - signed_R < 0)
            {
                image_shared[threadIdx.x] = 1;
            }
            else
            {
                image_shared[threadIdx.x] = image[i - signed_R];
            }
        }
        else if (blockDim.x - threadIdx.x <= signed_R) // Checking for the last R threads to move the extra R values at the end
        {
            if (i + signed_R >= n)
            {
                image_shared[threadIdx.x + 2 * signed_R] = 1;
            }
            else
            {
                image_shared[threadIdx.x + 2 * signed_R] = image[i + signed_R];
            }
        }
    }

    // Moving mask over
    if (threadIdx.x < 2 * signed_R + 1)
    {
        mask_shared[threadIdx.x] = mask[threadIdx.x];
    }
    __syncthreads();

    if (i >= n)
        return;
    
    for (int j = -1 * signed_R; j <= signed_R; j++)
    {
        output_shared[threadIdx.x] += image_shared[threadIdx.x + R + j] * mask_shared[j + signed_R];
    }
    output[i] = output_shared[threadIdx.x];
}

__host__ void stencil(const float *image,
                      const float *mask,
                      float *output,
                      unsigned int n,
                      unsigned int R,
                      unsigned int threads_per_block)
{
    int num_of_blocks = (n + threads_per_block - 1) / threads_per_block;
    // Shared image array will be of size threads_per_block + 2 * R
    // Shared mask array will be of size 2 * R + 1
    // Shared output array will be of size threads_per_block
    stencil_kernel<<<num_of_blocks, threads_per_block, sizeof(float) * (2 * threads_per_block + 4 * R + 1)>>>(image, mask, output, n, R);
    cudaDeviceSynchronize();
}