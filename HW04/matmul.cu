#include <cuda.h>
#include <stdio.h>
#include <random>
#include "matmul.cuh"

__global__ void matmul_kernel(const float* A, const float* B, float* C, size_t n) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int idx_x =  idx / n; // Row num of first matrix
    int idx_y = idx % n; // Col num of second matrix
    //idx_x, idx_y is the coordinates of the value in output matrix

    float sum = 0;
    for (int i = 0; i < n; i++)
    {
        sum += A[idx_x * n + i] * B[i * n + idx_y];
    }
    C[idx_x * n + idx_y] = sum;
}

void matmul(const float* A, const float* B, float* C, size_t n, unsigned int threads_per_block) {
    int nsquared = n * n;
    int num_of_blocks = (nsquared + threads_per_block - 1) / threads_per_block;
    matmul_kernel<<<num_of_blocks, threads_per_block>>>(A, B, C, n);
    cudaDeviceSynchronize();
}