#include <cuda.h>
#include <stdio.h>
#include <random>
#include "stencil.cuh"

using namespace std;

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 4) {
        printf("Usage: ./task2 n R threads_per_block");
        return 0;
    }

    // Grabbing device information for copying data between pointers
    int deviceId;
    cudaGetDevice(&deviceId);

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    float *image;
    float *mask;
    float *output;

    int n = atoi(argv[1]);
    int R = atoi(argv[2]);
    int mask_size = 2 * R + 1;
    int threads_per_block = atoi(argv[3]);

    cudaMalloc(&image, sizeof(float) * n);
    cudaMalloc(&mask, sizeof(float) * mask_size);
    cudaMalloc(&output, sizeof(float) * n);

    float *hImage;
    float *hMask;
    float *hOutput;

    cudaMallocHost(&hImage, sizeof(float) * n);
    cudaMallocHost(&hMask, sizeof(float) * mask_size);
    cudaMallocHost(&hOutput, sizeof(float) * n);

    // Creating random number generator
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dist(-1, 1);

    for (int i = 0; i < n; i++)
    {
        hImage[i] = dist(gen);
        if (i < mask_size) hMask[i] = dist(gen);
    }

    cudaMemcpy(image, hImage, sizeof(float) * n, cudaMemcpyHostToDevice);
    cudaMemcpy(mask, hMask, sizeof(float) * mask_size, cudaMemcpyHostToDevice);

    // Starting timer and calling kernel
    cudaEventRecord(start);    
    stencil(image, mask, output, n, R, threads_per_block);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Measuring time elapsed
    float ms;
    cudaEventElapsedTime(&ms, start, stop);

    // Copying result to host
    cudaMemcpy(hOutput, output, sizeof(float) * n, cudaMemcpyDeviceToHost);

    printf("%f\n%f\n", hOutput[n - 1], ms);
    
    cudaFree(image);
    cudaFree(mask);
    cudaFree(output);
    cudaFree(hImage);
    cudaFree(hMask);
    cudaFree(hOutput);
    return 0;
}
