#include <cuda.h>
#include "matmul.cuh"

template <typename T>
__global__ void matmul(const T *A, const T *B, T *C, unsigned int n)
{
    // Block index
    int bx = blockIdx.x; // the B (and C) matrix sub-block column index
    int by = blockIdx.y; // the A (and C) matrix sub-block row index
    // Thread index
    int tx = threadIdx.x; // the column index in the sub-block
    int ty = threadIdx.y; // the row index in the sub-block
    //if (tx + bx * blockDim.x >= n || ty + by * blockDim.x >= n) return;
    //  Index of the first sub-matrix of A processed by the block
    int aBegin = n * blockDim.x * by;
    // Index of the last sub-matrix of A processed by the block
    int aEnd = aBegin + n - 1;
    // Step size used to iterate through the sub-matrices of A
    int aStep = blockDim.x;
    // Index of the first sub-matrix of B processed by the block
    int bBegin = blockDim.x * bx;
    // Step size used to iterate through the sub-matrices of B
    int bStep = blockDim.x * n;
    // The element of the block sub-matrix that is computed
    // by the thread
    T Csub = 0;  

    // Shared memory for the sub-matrices (tiles) of A and B
    extern __shared__ int shared[];
    T* As = (T*)shared;
    T* Bs = (T*)&As[blockDim.x * blockDim.x];
    // Loop over all the sub-matrices (tiles) of A and B required to
    // compute the block sub-matrix; moving in A left to right in
    // a row, and in B from top to bottom in a column
    for (int a = aBegin, b = bBegin; a <= aEnd; a += aStep, b += bStep)
    {
        // Load tiles from global memory into shared memory; each
        // thread loads one element of the two tiles from A & B
        if (ty + by * blockDim.x < n) // Ensuring that no out of bounds error occurs when blocks don't align with matrix
            As[ty * blockDim.x + tx] = A[a + n * ty + tx];
        if (tx + bx * blockDim.x < n)
            Bs[ty * blockDim.x + tx] = B[b + n * ty + tx];
        // Synchronize to make sure the matrices are loaded
        __syncthreads();
        // Each thread in this block computes one element
        // of the block sub-matrix (tile). Thread with indexes
        // ty and tx computes in this tile the entry [ty][tx].
        if (ty + by * blockDim.x < n && tx + bx * blockDim.x < n) // Ensuring that no out of bounds error occurs when blocks don't align with matrix
            for (int k = 0; k < blockDim.x; ++k)
                Csub += As[ty * blockDim.x + k] * Bs[k * blockDim.x + tx];

        // Synchronize to make sure that the preceding
        // computation is done before loading two new
        // sub-matrices of A and B in the next iteration
        __syncthreads();
    }
    // Write the block sub-matrix to global memory;
    // each thread writes one element
    if (ty + by * blockDim.x < n && tx + bx * blockDim.x < n) { // Ensuring that no out of bounds error occurs when blocks don't align with matrix
        int c = n * blockDim.x * by + blockDim.x * bx;
        C[c + n * ty + tx] = Csub;
    }
}

dim3 computeNumOfBlocks(int n, int block_dim)
{
    return dim3((n + block_dim - 1) / block_dim, (n + block_dim - 1) / block_dim);
}

__host__ void matmul_1(const int *A, const int *B, int *C, unsigned int n, unsigned int block_dim)
{
    matmul<<<computeNumOfBlocks(n, block_dim), dim3(block_dim, block_dim), 2 * block_dim * block_dim * sizeof(int)>>>(A, B, C, n);
    cudaDeviceSynchronize();
}

__host__ void matmul_2(const float *A, const float *B, float *C, unsigned int n, unsigned int block_dim)
{
    matmul<<<computeNumOfBlocks(n, block_dim), dim3(block_dim, block_dim), 2 * block_dim * block_dim * sizeof(float)>>>(A, B, C, n);
    cudaDeviceSynchronize();
}

__host__ void matmul_3(const double *A, const double *B, double *C, unsigned int n, unsigned int block_dim)
{
    matmul<<<computeNumOfBlocks(n, block_dim), dim3(block_dim, block_dim), 2 * block_dim * block_dim * sizeof(double)>>>(A, B, C, n);
    cudaDeviceSynchronize();
}