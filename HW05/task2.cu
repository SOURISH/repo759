#include <cuda.h>
#include <stdio.h>
#include <random>
#include "matmul.cuh"

int main(int argc, char const *argv[])
{

    // Checking that correct number of command line arguments were passed in
    if (argc != 3)
    {
        printf("Usage: ./task2 N block_dim");
        return 0;
    }

    int N = atoi(argv[1]);
    int size = N * N;
    int block_dim = atoi(argv[2]);

    // Grabbing device information for copying data between pointers
    int deviceId;
    cudaGetDevice(&deviceId);

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    int *A_int;
    int *B_int;
    int *C_int;

    float *A_float;
    float *B_float;
    float *C_float;

    double *A_double;
    double *B_double;
    double *C_double;

    cudaMallocManaged(&A_int, sizeof(int) * size);
    cudaMallocManaged(&B_int, sizeof(int) * size);
    cudaMallocManaged(&C_int, sizeof(int) * size);

    // Creating random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dist_int(0, 3);
    std::uniform_real_distribution<double> dist(-1, 1);

    // Initalizing the two matrices that'll be multiplied
    for (int i = 0; i < size; i++)
    {
        A_int[i] = ((int)dist_int(gen)) - 1;
        B_int[i] = ((int)dist_int(gen)) - 1;
    }

    // Moving randomly generated numbers to device to prevent page faulting
    cudaMemPrefetchAsync(A_int, sizeof(int) * size, deviceId);
    cudaMemPrefetchAsync(B_int, sizeof(int) * size, deviceId);
    cudaMemPrefetchAsync(C_int, sizeof(int) * size, deviceId);
    cudaDeviceSynchronize();
    
    ////////////////////////////////////////////////////////
    // Starting timer and calling kernel for int matrices
    cudaEventRecord(start);
    matmul_1(A_int, B_int, C_int, N, block_dim);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Measuring time elapsed
    float ms;
    cudaEventElapsedTime(&ms, start, stop);

    printf("%d\n%d\n%f\n", C_int[0], C_int[size - 1], ms);

    cudaFree(A_int);
    cudaFree(B_int);
    cudaFree(C_int);

    ////////////////////////////////////////////////////////
    // FLOATS
    cudaMallocManaged(&A_float, sizeof(float) * size);
    cudaMallocManaged(&B_float, sizeof(float) * size);
    cudaMallocManaged(&C_float, sizeof(float) * size);

    // Movings floats back to host
    cudaMemPrefetchAsync(A_float, sizeof(float) * size, cudaCpuDeviceId);
    cudaMemPrefetchAsync(B_float, sizeof(float) * size, cudaCpuDeviceId);
    cudaMemPrefetchAsync(C_float, sizeof(float) * size, cudaCpuDeviceId);

    // Copying randomly generated values over to float arrays
    for (int i = 0; i < size; i++)
    {
        A_float[i] = dist(gen);
        B_float[i] = dist(gen);
    }

    // Moving floats over to device
    cudaMemPrefetchAsync(A_float, sizeof(float) * size, deviceId);
    cudaMemPrefetchAsync(B_float, sizeof(float) * size, deviceId);
    cudaMemPrefetchAsync(C_float, sizeof(float) * size, deviceId);

    // Starting timer and calling kernel for float matrices
    cudaEventRecord(start);
    matmul_2(A_float, B_float, C_float, N, block_dim);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Measuring time elapsed
    cudaEventElapsedTime(&ms, start, stop);

    printf("%f\n%f\n%f\n", C_float[0], C_float[size - 1], ms);
    cudaFree(A_float); // Freeing floats to save space
    cudaFree(B_float);
    cudaFree(C_float);

    ////////////////////////////////////////////////////////
    // DOUBLES
    cudaMallocManaged(&A_double, sizeof(double) * size);
    cudaMallocManaged(&B_double, sizeof(double) * size);
    cudaMallocManaged(&C_double, sizeof(double) * size);
    
    // Randomly generating doubles
    for (int i = 0; i < size; i++)
    {
        A_double[i] = dist(gen);
        B_double[i] = dist(gen);
    }

    // Moving doubles to device
    cudaMemPrefetchAsync(A_double, sizeof(double) * size, deviceId);
    cudaMemPrefetchAsync(B_double, sizeof(double) * size, deviceId);
    cudaMemPrefetchAsync(C_double, sizeof(double) * size, deviceId);
    cudaDeviceSynchronize();

    // Starting timer and calling kernel for double matrices
    cudaEventRecord(start);
    matmul_3(A_double, B_double, C_double, N, block_dim);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Measuring time elapsed
    cudaEventElapsedTime(&ms, start, stop);

    printf("%lf\n%lf\n%f\n", C_double[0], C_double[size - 1], ms);

    cudaFree(A_double);
    cudaFree(B_double);
    cudaFree(C_double);
    return 0;
}
