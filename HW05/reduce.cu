#include <cuda.h>
#include "reduce.cuh"

__host__ void reduce(float** input, float** output, unsigned int N, unsigned int threads_per_block)
{
    int signed_N = N;
    while (true)
    {
        int num_of_blocks = ((signed_N + 1) / 2 + (int)threads_per_block - 1) / (int)threads_per_block;        
        reduce_kernel << <num_of_blocks, threads_per_block, threads_per_block * sizeof(float) >> > (*input, *output, signed_N);

        if (num_of_blocks <= 1) break;
        
        // Copying the appropriate number of values from the output array back into the input array for the next loop iteration
        signed_N = num_of_blocks;
        cudaMemcpy(*input, *output, sizeof(float) * signed_N, cudaMemcpyDeviceToDevice);
    }
    cudaDeviceSynchronize();
}

__global__ void reduce_kernel(float* g_idata, float* g_odata, unsigned int n)
{
    extern __shared__ float sdata[];
    int val; // We need to "cap" the number of active threads if n is less than the blockDim.x
    if (n < blockDim.x)
        val = n;
    else
        val = blockDim.x;

    // each thread loads one element from global to shared mem
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x * (val * 2) + threadIdx.x; // Val will be blockDim.x usually except when n is less than blockDim.x

    if (i < n)
    {
        if (i + val < n) {
            sdata[tid] = g_idata[i] + g_idata[i + val];
        }
        else {
            sdata[tid] = g_idata[i]; // Ensuring we don't pull invalid entries for when n is odd
        }
        
        __syncthreads();
        // This for loop has been modified for accomodate for an odd number appearing at any point when dividing by two
        // 1024 -> 512 -> 256 -> 128 -> 64 -> 32 -> 16 -> 8 -> 4 -> 2 -> 1 (All good!)
        // 10000 -> 5000 -> 2500 -> 1250 -> 625 -> (We now need to do (625 + 1) / 2 so we get 313 instead of 312!)
        for (unsigned int s = (val + 1) / 2; s > 0; s >>= 1) {
            if (tid < s && tid + s < n) {
                sdata[tid] += sdata[tid + s];
            }
            n = s; // Ensuring that invalid entries will not be grabbed when n is odd
            if (s > 2) s++; // Ensuring last element of n isn't missed when n is odd
            __syncthreads();
        }
        if (tid == 0)
            g_odata[blockIdx.x] = sdata[0];
    }
}