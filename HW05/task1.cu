#include <cuda.h>
#include <stdio.h>
#include <random>
#include "reduce.cuh"

// using namespace std;

int main(int argc, char const *argv[])
{
    // Checking that correct number of command line arguments were passed in
    if (argc != 3)
    {
        printf("Usage: ./task1 N threads_per_block");
        return 0;
    }

    // Creating objects for timers
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    float *input;
    float *output;

    int N = atoi(argv[1]);
    int threads_per_block = atoi(argv[2]);

    cudaMalloc(&input, sizeof(float) * N);
    cudaMalloc(&output, sizeof(float) * N);

    float *hInput;
    float *hOutput;

    cudaMallocHost(&hInput, sizeof(float) * N);
    cudaMallocHost(&hOutput, sizeof(float) * N);

    // Creating random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dist(-1, 1);

    // Initalizing the two matrices that'll be multiplied
    for (int i = 0; i < N; i++)
        hInput[i] = dist(gen);

    // Copying randomly generated numbers to device
    cudaMemcpy(input, hInput, sizeof(float) * N, cudaMemcpyHostToDevice);

    // Starting timer and calling kernel
    cudaEventRecord(start);
    reduce(&input, &output, N, threads_per_block);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Measuring time elapsed
    float ms;
    cudaEventElapsedTime(&ms, start, stop);

    // Copying result to host
    cudaMemcpy(hOutput, output, sizeof(float) * N, cudaMemcpyDeviceToHost);

    printf("%f\n%f\n", hOutput[0], ms);

    cudaFree(input);
    cudaFree(output);
    cudaFree(hInput);
    cudaFree(hOutput);
    return 0;
}
