#include <random>
#include <chrono>
#include "matmul.h"

using namespace std;

using std::chrono::duration;
using std::chrono::high_resolution_clock;

void populateMatrices(double* matrix, vector<double>& matrix_vec, int n) {
    /**
     * matrix is the pointer to the matrix
     * n is the number of rows (and columns)
     * 
     */
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dist(0, 10);
    for (size_t i = 0; i < n * n; i++)
    {
        matrix[i] = dist(gen);
        matrix_vec.push_back(matrix[i]);
    }
    
}

void clearOutput(double * matrix, int n) {
    for (size_t i = 0; i < n * n; i++)
    {
        matrix[i] = 0;
    }
    
}

int main(int argc, char const *argv[])
{
    high_resolution_clock::time_point start;
    high_resolution_clock::time_point end;
    duration<double, std::milli> duration_sec;

    int n = 1024;
    printf("%d\n", n);
    
    double * A = (double *) malloc(sizeof(double) * n * n);
    double * B = (double *) malloc(sizeof(double) * n * n);
    double * C = (double *) malloc(sizeof(double) * n * n);

    vector<double> A_vec;
    vector<double> B_vec;

    populateMatrices(A, A_vec, n);
    populateMatrices(B, B_vec, n);

    start = high_resolution_clock::now();
    mmul1(A, B, C, n);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli> >(end - start);
    printf("%f\n%f\n", C[n * n - 1], duration_sec.count());
    free(C);
    C = (double *) malloc(sizeof(double) * n * n);
    clearOutput(C, n);

    start = high_resolution_clock::now();
    mmul2(A, B, C, n);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli> >(end - start);
    printf("%f\n%f\n", C[n * n - 1], duration_sec.count());
    clearOutput(C, n);

    start = high_resolution_clock::now();
    mmul3(A, B, C, n);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli> >(end - start);
    printf("%f\n%f\n", C[n * n - 1], duration_sec.count());
    clearOutput(C, n);

    start = high_resolution_clock::now();
    mmul4(A_vec, B_vec, C, n);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli> >(end - start);
    printf("%f\n%f\n", C[n * n - 1], duration_sec.count());

    free(A);
    free(B);
    return 0;
}
