#include <cstdlib>
#include <stdlib.h>
#include <chrono>
#include <random>
#include "scan.h"

using namespace std;

using std::chrono::duration;
using std::chrono::high_resolution_clock;

int main(int argc, char const *argv[])
{
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dist(-1, 1);

    high_resolution_clock::time_point start;
    high_resolution_clock::time_point end;
    duration<double, std::milli> duration_sec;

    int n = atoi(argv[1]);
    //printf("%d\n\n", n);
    float * arr = (float *) malloc(sizeof(float) * n);
    for (int i = 0; i < n; i++)
    {
        arr[i] = dist(gen);
    }
    
    float * output = (float *) malloc(sizeof(float) * n);

    start = high_resolution_clock::now();
    scan(arr, output, n);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli> >(end - start);

    printf("%f\n%f\n%f\n", duration_sec.count(), output[0], output[n - 1]);

    free(arr);
    free(output);
    return 0;
}
