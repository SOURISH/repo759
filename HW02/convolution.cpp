#include "convolution.h"

void convolve(const float *image, float *output, std::size_t n, const float *mask, std::size_t m) {
    for (size_t x = 0; x < n; x++) // image rows
    {
        for (size_t y = 0; y < n; y++) // image cols
        {
            float maskResult = 0;
            for (size_t i = 0; i < m; i++) // mask rows
            {
                for (size_t j = 0; j < m; j++) // mask columns
                {
                    auto imgXdim = x + i - ((m - 1) / 2); // Calculating row dimension
                    auto imgYdim = y + j - ((m - 1) / 2); // Calculating column dimension

                    auto cond1 = imgXdim >= 0 && imgXdim < n; // Checking if row index is within bounds
                    auto cond2 = imgYdim >= 0 && imgYdim < n; // Checking if column index is within bounds
                    
                    float imgVal;
                    if (!cond1 && !cond2) { // Both conditions aren't met
                        imgVal = 0;
                    } else if (!cond1 || !cond2) { // One condition isn't met
                        imgVal = 1;
                    } else { // Both conditions are met
                        imgVal = image[imgXdim * n + imgYdim];
                    }
                    maskResult += mask[i * m + j] * imgVal;
                }
            }
            output[x * n + y] = maskResult;
        }
    }
}
