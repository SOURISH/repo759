#include <cstdlib>
#include <stdlib.h>
#include <chrono>
#include <random>
#include "convolution.h"

using namespace std;

using std::chrono::duration;
using std::chrono::high_resolution_clock;

int main(int argc, char const *argv[])
{
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dist_image(-10, 10);
    uniform_real_distribution<double> dist_mask(-1, 1);

    high_resolution_clock::time_point start;
    high_resolution_clock::time_point end;
    duration<double, std::milli> duration_sec;

    int n = atoi(argv[1]);
    int m = atoi(argv[2]);

    float * image = (float *) malloc(sizeof(float) * n * n);
    float * output = (float *) malloc(sizeof(float) * n * n);
    float * mask = (float *) malloc(sizeof(float) * m * m);

    for (size_t i = 0; i < n * n; i++)
    {
        image[i] = dist_image(gen);
    }

    for (size_t i = 0; i < m * m; i++)
    {
        mask[i] = dist_mask(gen);
    }

    start = high_resolution_clock::now();
    convolve(image, output, n, mask, m);
    end = high_resolution_clock::now();
    duration_sec = std::chrono::duration_cast<duration<double, std::milli> >(end - start);
    printf("%f\n%f\n%f\n", duration_sec.count(), output[0], output[n * n - 1]);

    delete image;
    delete output;
    delete mask;
    return 0;
}
